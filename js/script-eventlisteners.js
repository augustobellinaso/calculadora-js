const display = document.getElementById("displayVisor");

// Adicionando um listener nos botões dos valores
addGlobalEventListener("click", "button.valor", e => {
    if (textoDisplayArray().length <= 3)  {
        escreverNaTela(e.target.value);
    }  
});

//Listener para os botões com operações
addGlobalEventListener("click", "button.operacao", e => {    
    if (textoDisplay() != "" && textoDisplayArray().length < 3) {
        escreverNaTela(" ");
        escreverNaTela(e.target.value);
        escreverNaTela(" ");
    }
});

//Listener para o backspace
addGlobalEventListener("click", "button.backspace", () => {    
    apagarUltimoDigito();
});

//Listener para apagar tudo
addGlobalEventListener("click", "button.apagarTudo", () => {
    apagarTudo();
});

//Listener para o resultado
addGlobalEventListener("click", "button.resultado", e => {
    if (textoDisplayArray().length == 3) {
        let valor1 = parseFloat(textoDisplayArray()[0]);
        let valor2 = parseFloat(textoDisplayArray()[2]);
        let operacao = textoDisplayArray()[1];


        escreverNaTela(" ");
        escreverNaTela(e.target.value);
        escreverNaTela(" ");
        escreverNaTela(realizarOperacao(operacao, valor1, valor2));
    }   
})

//Listener para o separador decimal
addGlobalEventListener("click", "button.decimal", e => { 
    const ponto = e.target.value;
    if (textoDisplayArray().length <= 3)  {
        if (textoDisplayArray().length == 1) {
            if (!textoDisplayArray()[0].includes(ponto)) {
                escreverNaTela(ponto);                
            }
        } else if (textoDisplayArray().length == 3) {
            if (!textoDisplayArray()[0].includes(ponto) || !textoDisplayArray()[2].includes(ponto)) {
                escreverNaTela(ponto);
            }
        }
    }  
})

//Funções específicas
apagarTudo = () => {
    display.value = "";
}

apagarUltimoDigito = () => {
    if (textoDisplayArray().length <= 3) {
        display.value = display.value.slice(0, -1);
    }
}

//Funções auxiliares
textoDisplay = () => {
    return display.value;
}

textoDisplayArray = () => {
    return textoDisplay().split(" ");
}

escreverNaTela = (texto) => {
    display.value += texto;
}


realizarOperacao = (operacao, a, b) => {
    if (operacao == "+") {
        return somar(a, b);
    } else if (operacao == "-") {
        return subtrair(a, b);
    } else if (operacao == "*") {
        return multiplicar(a, b);
    } else if (operacao == "/") {
        return dividir(a, b);
    }
}


// Funções de cada operação
somar = (a, b) => a + b;
subtrair = (a, b) => a - b;
dividir = (a, b) => a / b;
multiplicar = (a, b) => a * b;




function addGlobalEventListener(type, selector, callback) {
    document.addEventListener(type, evt => {
        if (evt.target.matches(selector)) {
            callback(evt);
        }
    })
}