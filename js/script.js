getValor = (element) => {
    let valor = element.value

    if (obterInput().length < 5) {
        escreverNaTela(valor);
    }
}

getDecimal = (element) => {
    let valor = element.value

    let input = obterInput();

    if (input.length < 5) {

        if (input.length == 1) {
            if (!input[0].includes(valor)) {
                escreverNaTela(valor);        
           }
        } else if (input.length == 3) {
            if (!input[0].includes(valor) || !input[2].includes(valor)) {
                escreverNaTela(valor);        
           }
        }
        
    }
}

escreverNaTela = (valor) => {
    document.getElementById("displayVisor").value += valor;
}

getOperacao = (element) => {
    let operacao = element.value;
    if (obterInput() != "" && obterInput().length < 3) {
        document.getElementById("displayVisor").value += " " + operacao + " ";
    }
}

calcularResposta = () => {   
    if (obterInput().length == 3) {
        let valor1 = parseFloat(obterInput()[0]);
        let valor2 = parseFloat(obterInput()[2]);
        let operacao = obterInput()[1];
        document.getElementById("displayVisor").value += " = " + realizarOperacao(operacao, valor1, valor2);
        
    }
}

limparUltimoDigito = () => {
    if (obterInput().length <= 3) {
        let texto = obterInputString();
        texto = texto.slice(0, -1);
        document.getElementById("displayVisor").value = texto;
    }
}

limpar = () => {
    document.getElementById("displayVisor").value = "";
}

// Função que pega o texto presente na tela e divide ele em um array com o espaço como delimitador
obterInput = () => {
    let texto = document.getElementById("displayVisor").value;
    return texto.split(" ");
}

obterInputString = () => {
    return document.getElementById("displayVisor").value;
}



// FUnção para realizar as operações
realizarOperacao = (operacao, a, b) => {
    if (operacao == "+") {
        return somar(a, b);
    } else if (operacao == "-") {
        return subtrair(a, b);
    } else if (operacao == "*") {
        return multiplicar(a, b);
    } else if (operacao == "/") {
        return dividir(a, b);
    } else {
        return "Operação inválida";
    }
}


// Funções de cada operação
somar = (a, b) => a + b;
subtrair = (a, b) => a - b;
dividir = (a, b) => a / b;
multiplicar = (a, b) => a * b;
